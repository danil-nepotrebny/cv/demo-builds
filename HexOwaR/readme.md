HexOwaR is a cross platform multiplayer strategy game made with Unity3D.

A dystopian future. Regular sports has become obsolete. Now there's HexoWar - a fresh ruthless digital competitive discipline. You are a HexoWar team manager. Your first and foremost allies are tactics and champions of the sectors.

Key features list:
* Cross-Platform network gameplay (this build networking is outdated so it can't be run, but it was back in a days)
* Futuristic cyberpunk setting
* Noir-based stylization
* Various multiplayer modes
* Story-driven gameplay
* Platform-based ranking system
* Wide range of tactics with ChampionSkills system

This is an early beta build. As a developer I was taking part in almost every part of the project: Sound system, UI, core gameplay, networking etc.

Teaser: https://www.youtube.com/watch?v=OyejylMJDAM
Review: https://www.youtube.com/watch?v=6LgeD98-Tq8

