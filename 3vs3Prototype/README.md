3vs3 prototype game made at Heart Beat Games. It's something like "micro moba", first try to use Unity UNET system and proof of concept.
My code, game design by Danil Goshko, assets by Danil Goshko / Blizzard Entertainment.

### How to:
1. It's still playabe with localhost or local network
2. Open the game and create the room
3. Open second client and select find game
4. Select and join room

### Controls:
1. Use 1, 2, 3, buttons on keyboard to select character
2. Use Left mouse button to point selected character to the target (attack/move)
3. Use Q, W, E keyboard to use second ability on each character corresponding 1, 2, 3 